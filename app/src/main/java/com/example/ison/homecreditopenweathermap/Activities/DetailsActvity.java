package com.example.ison.homecreditopenweathermap.Activities;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ison.homecreditopenweathermap.POJO.ApplicationGlobalVariables;
import com.example.ison.homecreditopenweathermap.R;
import com.example.ison.homecreditopenweathermap.Utils.ApiUtils;
import com.squareup.picasso.Picasso;

public class DetailsActvity extends AppCompatActivity {

    private TextView txtCity, txtID, txtMain, txtDescription, txtIcon, txtTemp, txtPressure, txtHumidity, txtTempMin, txtTempMax;
    private ImageView imgView;
    private ApplicationGlobalVariables applicationGlobalVariables;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_actvity);
        applicationGlobalVariables = (ApplicationGlobalVariables) getApplication();

        txtCity = (TextView) findViewById(R.id.textCity);
        txtCity.setText("City: " + applicationGlobalVariables.getCity());
        txtID = (TextView) findViewById(R.id.textID);
        txtID.setText("ID: " + applicationGlobalVariables.getId());
        txtMain = (TextView) findViewById(R.id.textMain);
        txtMain.setText("Weather: " + applicationGlobalVariables.getMain());
        txtDescription = (TextView) findViewById(R.id.textDescription);
        txtDescription.setText("Description: " + applicationGlobalVariables.getDescription());
        txtIcon = (TextView) findViewById(R.id.textIcon);
        txtIcon.setText("Icon: " + applicationGlobalVariables.getIcon());
        imgView = (ImageView) findViewById(R.id.imgView);
        Picasso.with(this).load(ApiUtils.DOMAIN+ApiUtils.URL_ICON+applicationGlobalVariables.getIcon()).into(imgView);
        txtTemp = (TextView) findViewById(R.id.textTemp);
        txtTemp.setText("Temperature: "+applicationGlobalVariables.getTemp());
        txtPressure = (TextView) findViewById(R.id.textPressure);
        txtPressure.setText("Pressure: " + applicationGlobalVariables.getPressure());
        txtHumidity = (TextView) findViewById(R.id.textHumidity);
        txtHumidity.setText("Humidity: " + applicationGlobalVariables.getHumidity());
        txtTempMin = (TextView) findViewById(R.id.textTempMin);
        txtTempMin.setText("Min Temp: " + applicationGlobalVariables.getTempMin());
        txtTempMax = (TextView) findViewById(R.id.textTempMax);
        txtTempMax.setText("Max Temp: " + applicationGlobalVariables.getTempMax());
    }

    @Override
    public void onBackPressed() {
            Intent myIntent = new Intent(this, MainActivity.class);
            this.finish();
            startActivity(myIntent);
    }
}
