package com.example.ison.homecreditopenweathermap.Activities;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.ison.homecreditopenweathermap.Constants.CommonConstants;
import com.example.ison.homecreditopenweathermap.R;

import gr.net.maroulis.library.EasySplashScreen;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EasySplashScreen config = new EasySplashScreen((SplashScreen.this))
                .withFullScreen()
                .withTargetActivity(MainActivity.class)
                .withSplashTimeOut(2000)
                .withBackgroundColor(Color.parseColor("#0000ff"))
                .withLogo(R.mipmap.ic_launcher_round)
                .withAfterLogoText(CommonConstants.BUILD);


        config.getAfterLogoTextView().setTextColor(Color.WHITE);

        View view = config.create();
        setContentView(view);
    }
}
