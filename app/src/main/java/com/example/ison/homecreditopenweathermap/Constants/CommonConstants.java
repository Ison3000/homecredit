package com.example.ison.homecreditopenweathermap.Constants;


import com.example.ison.homecreditopenweathermap.BuildConfig;

public class CommonConstants {
    public static final String BUILD = BuildConfig.BUILD;
    public static final String APP_ID = BuildConfig.APP_ID;
    public static final String londonID = "London,uk";
    public static final String pragueID = "Prague,cz";
    public static final String sanFranciscoID = "San Francisco,us";
}
