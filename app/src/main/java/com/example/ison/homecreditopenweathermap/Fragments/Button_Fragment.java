package com.example.ison.homecreditopenweathermap.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.ison.homecreditopenweathermap.API.Api;
import com.example.ison.homecreditopenweathermap.Activities.DetailsActvity;
import com.example.ison.homecreditopenweathermap.Constants.CommonConstants;
import com.example.ison.homecreditopenweathermap.POJO.ApplicationGlobalVariables;
import com.example.ison.homecreditopenweathermap.POJO.ErrorModel;
import com.example.ison.homecreditopenweathermap.POJO.RequestWeatherResponse;
import com.example.ison.homecreditopenweathermap.R;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Button_Fragment extends Fragment implements View.OnClickListener{
    private Gson gson;
    private ErrorModel errorModel;
    private Button btn_london;
    private Button btn_pragaue;
    private Button btn_san_francisco;
    private Button btn_refesh;
    private ApplicationGlobalVariables applicationGlobalVariables;

    public Button_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_button_, container, false);
        applicationGlobalVariables = (ApplicationGlobalVariables) getActivity().getApplication();
        btn_london = (Button) mView.findViewById(R.id.btn_london);
        btn_pragaue = (Button) mView.findViewById(R.id.btn_prague);
        btn_san_francisco = (Button) mView.findViewById(R.id.btn_san_francisco);
        btn_refesh = (Button) mView.findViewById(R.id.btn_refresh);
        btn_refesh.setOnClickListener(this);
        btn_london.setOnClickListener(this);
        btn_pragaue.setOnClickListener(this);
        btn_san_francisco.setOnClickListener(this);

        refresh();

        return  mView;
    }

    private void refresh(){
        getWeatherRequest(CommonConstants.londonID, false);
        getWeatherRequest(CommonConstants.pragueID, false);
        getWeatherRequest(CommonConstants.sanFranciscoID, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_london:
                getWeatherRequest(CommonConstants.londonID, true);
                break;
            case R.id.btn_prague:
                getWeatherRequest(CommonConstants.pragueID, true);
                break;
            case R.id.btn_san_francisco:
                getWeatherRequest(CommonConstants.sanFranciscoID, true);
                break;
            case R.id.btn_refresh:
                Log.e("aaaaa", "onClick: btn_refresh" );
                refresh();
                break;
        }
    }


    public void openActivity(){
        Intent myIntent = new Intent(getActivity(), DetailsActvity.class);
        getActivity().finish();
        getActivity().startActivity(myIntent);
    }

    private void getWeatherRequest(final String id, final Boolean openNew){

        Api.getClient().requestWeather(id, CommonConstants.APP_ID).enqueue(new Callback<RequestWeatherResponse>() {
            @Override
            public void onResponse(Call<RequestWeatherResponse> call, Response<RequestWeatherResponse> response) {
                if(response.isSuccessful()){
                    switch (id){
                        case CommonConstants.londonID:
                            btn_london.setText("London\nlongitude "+ response.body().getCoord().getLon() + "\nlatitude "
                            +response.body().getCoord().getLat() + "\nweather " +  response.body().getWeather().get(0).getDescription()
                            + "\nTemperature " + response.body().getMain().getTemp());
                            applicationGlobalVariables.setCity("London");
                            applicationGlobalVariables.setId(response.body().getWeather().get(0).getId().toString());
                            applicationGlobalVariables.setMain(response.body().getWeather().get(0).getMain());
                            applicationGlobalVariables.setDescription(response.body().getWeather().get(0).getDescription());
                            applicationGlobalVariables.setIcon(response.body().getWeather().get(0).getIcon());
                            applicationGlobalVariables.setTemp(response.body().getMain().getTemp().toString());
                            applicationGlobalVariables.setHumidity(response.body().getMain().getHumidity().toString());
                            applicationGlobalVariables.setPressure(response.body().getMain().getPressure().toString());
                            applicationGlobalVariables.setTempMax(response.body().getMain().getTempMax().toString());
                            applicationGlobalVariables.setTempMin(response.body().getMain().getTempMin().toString());
                            if(openNew){
                                openActivity();
                            }
                            break;
                        case CommonConstants.pragueID:
                            btn_pragaue.setText("Prague\nlongitude "+ response.body().getCoord().getLon() + "\nlatitude "
                                    +response.body().getCoord().getLat() + "\nweather " +  response.body().getWeather().get(0).getMain()
                                    + "\nTemperature " + response.body().getMain().getTemp());
                            applicationGlobalVariables.setCity("Prague");
                            applicationGlobalVariables.setId(response.body().getWeather().get(0).getId().toString());
                            applicationGlobalVariables.setMain(response.body().getWeather().get(0).getMain());
                            applicationGlobalVariables.setDescription(response.body().getWeather().get(0).getDescription());
                            applicationGlobalVariables.setIcon(response.body().getWeather().get(0).getIcon());
                            applicationGlobalVariables.setTemp(response.body().getMain().getTemp().toString());
                            applicationGlobalVariables.setHumidity(response.body().getMain().getHumidity().toString());
                            applicationGlobalVariables.setPressure(response.body().getMain().getPressure().toString());
                            applicationGlobalVariables.setTempMax(response.body().getMain().getTempMax().toString());
                            applicationGlobalVariables.setTempMin(response.body().getMain().getTempMin().toString());
                            if(openNew){
                                openActivity();
                            }
                            break;
                        case CommonConstants.sanFranciscoID:
                            btn_san_francisco.setText("San Francisco\nlongitude "+ response.body().getCoord().getLon() + "\nlatitude "
                                    +response.body().getCoord().getLat() + "\nweather " +  response.body().getWeather().get(0).getMain()
                                    + "\nTemperature " + response.body().getMain().getTemp());
                            applicationGlobalVariables.setCity("San Francisco");
                            applicationGlobalVariables.setId(response.body().getWeather().get(0).getId().toString());
                            applicationGlobalVariables.setMain(response.body().getWeather().get(0).getMain());
                            applicationGlobalVariables.setDescription(response.body().getWeather().get(0).getDescription());
                            applicationGlobalVariables.setIcon(response.body().getWeather().get(0).getIcon());
                            applicationGlobalVariables.setTemp(response.body().getMain().getTemp().toString());
                            applicationGlobalVariables.setHumidity(response.body().getMain().getHumidity().toString());
                            applicationGlobalVariables.setPressure(response.body().getMain().getPressure().toString());
                            applicationGlobalVariables.setTempMax(response.body().getMain().getTempMax().toString());
                            applicationGlobalVariables.setTempMin(response.body().getMain().getTempMin().toString());
                            if(openNew){
                                openActivity();
                            }
                            break;
                        default:
                            break;
                    }
                }
                else {
                    try {
                        errorModel = new ErrorModel();
                        gson = new Gson();
                        errorModel = gson.fromJson(response.errorBody().string(), ErrorModel.class);
                    } catch (Exception e) {
                    }
                }
            }

            @Override
            public void onFailure(Call<RequestWeatherResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Failed",
                        Toast.LENGTH_LONG).show();
            }
        });

    }
}
