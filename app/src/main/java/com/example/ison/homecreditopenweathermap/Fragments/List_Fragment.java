package com.example.ison.homecreditopenweathermap.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ison.homecreditopenweathermap.API.Api;
import com.example.ison.homecreditopenweathermap.Activities.DetailsActvity;
import com.example.ison.homecreditopenweathermap.Constants.CommonConstants;
import com.example.ison.homecreditopenweathermap.POJO.ApplicationGlobalVariables;
import com.example.ison.homecreditopenweathermap.POJO.ErrorModel;
import com.example.ison.homecreditopenweathermap.POJO.RequestWeatherResponse;
import com.example.ison.homecreditopenweathermap.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class List_Fragment extends Fragment implements View.OnClickListener{
    private Gson gson;
    private ErrorModel errorModel;
    private ListView listView;
    private ArrayList<String> details;
    private Button btn_refesh;
    private ApplicationGlobalVariables applicationGlobalVariables;
    public List_Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_list_, container, false);
        applicationGlobalVariables = (ApplicationGlobalVariables) getActivity().getApplication();
        listView = (ListView) mView.findViewById(R.id.list);
        btn_refesh = (Button) mView.findViewById(R.id.btn_refresh);

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        details = new ArrayList<>();
        refresh();
        final ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,details);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                switch (i){
                    case 0:
                        getWeatherRequest(CommonConstants.londonID, true);
                        openActivity();
                        break;

                    case 1:
                        getWeatherRequest(CommonConstants.pragueID, true);
                        getActivity().finish();
                        openActivity();
                        break;

                    case 2:
                        getWeatherRequest(CommonConstants.sanFranciscoID, true);
                        getActivity().finish();
                        openActivity();
                        break;
                    default:
                        break;
                }

            }
        });
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_refresh:
                Log.e("aaaaa", "onClick: btn_refresh" );
                refresh();
                break;
        }
    }

    private void refresh(){
        getWeatherRequest(CommonConstants.londonID, false);
        getWeatherRequest(CommonConstants.pragueID, false);
        getWeatherRequest(CommonConstants.sanFranciscoID, false);
    }

    public void openActivity(){
        Intent myIntent = new Intent(getActivity(), DetailsActvity.class);
        getActivity().finish();
        getActivity().startActivity(myIntent);
    }

    private void getWeatherRequest(final String id, final Boolean openNew){

        Api.getClient().requestWeather(id, CommonConstants.APP_ID).enqueue(new Callback<RequestWeatherResponse>() {
            @Override
            public void onResponse(Call<RequestWeatherResponse> call, Response<RequestWeatherResponse> response) {
                if(response.isSuccessful()){
                    Log.e("aaaaa", "onResponse: " + response.body().getCoord().getLon() + " " + response.body().getCoord().getLat()
                            + " " + response.body().getWeather().get(0).getDescription() + " " + response.body().getMain().getTemp());
//                    Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_LONG).show();

                    switch (id){
                        case CommonConstants.londonID:
                            details.add(("London\nlongitude "+ response.body().getCoord().getLon() + "\nlatitude "
                                    +response.body().getCoord().getLat() + "\nweather " +  response.body().getWeather().get(0).getDescription()
                                    + "\nTemperature " + response.body().getMain().getTemp()));
                            applicationGlobalVariables.setCity("London");
                            applicationGlobalVariables.setId(response.body().getWeather().get(0).getId().toString());
                            applicationGlobalVariables.setMain(response.body().getWeather().get(0).getMain());
                            applicationGlobalVariables.setDescription(response.body().getWeather().get(0).getDescription());
                            applicationGlobalVariables.setIcon(response.body().getWeather().get(0).getIcon());
                            applicationGlobalVariables.setTemp(response.body().getMain().getTemp().toString());
                            applicationGlobalVariables.setHumidity(response.body().getMain().getHumidity().toString());
                            applicationGlobalVariables.setPressure(response.body().getMain().getPressure().toString());
                            applicationGlobalVariables.setTempMax(response.body().getMain().getTempMax().toString());
                            applicationGlobalVariables.setTempMin(response.body().getMain().getTempMin().toString());
                            if(openNew){
                                openActivity();
                            }
                            break;
                        case CommonConstants.pragueID:
                            details.add(("Prague\nlongitude "+ response.body().getCoord().getLon() + "\nlatitude "
                                    +response.body().getCoord().getLat() + "\nweather " +  response.body().getWeather().get(0).getDescription()
                                    + "\nTemperature " + response.body().getMain().getTemp()));
                            applicationGlobalVariables.setCity("Prague");
                            applicationGlobalVariables.setId(response.body().getWeather().get(0).getId().toString());
                            applicationGlobalVariables.setMain(response.body().getWeather().get(0).getMain());
                            applicationGlobalVariables.setDescription(response.body().getWeather().get(0).getDescription());
                            applicationGlobalVariables.setIcon(response.body().getWeather().get(0).getIcon());
                            applicationGlobalVariables.setTemp(response.body().getMain().getTemp().toString());
                            applicationGlobalVariables.setHumidity(response.body().getMain().getHumidity().toString());
                            applicationGlobalVariables.setPressure(response.body().getMain().getPressure().toString());
                            applicationGlobalVariables.setTempMax(response.body().getMain().getTempMax().toString());
                            applicationGlobalVariables.setTempMin(response.body().getMain().getTempMin().toString());
                            if(openNew){
                                openActivity();
                            }
                            break;

                        case CommonConstants.sanFranciscoID:
                            details.add(("San Francisco\nlongitude "+ response.body().getCoord().getLon() + "\nlatitude "
                                    +response.body().getCoord().getLat() + "\nweather " +  response.body().getWeather().get(0).getDescription()
                                    + "\nTemperature " + response.body().getMain().getTemp()));
                            applicationGlobalVariables.setCity("San Francisco");
                            applicationGlobalVariables.setId(response.body().getWeather().get(0).getId().toString());
                            applicationGlobalVariables.setMain(response.body().getWeather().get(0).getMain());
                            applicationGlobalVariables.setDescription(response.body().getWeather().get(0).getDescription());
                            applicationGlobalVariables.setIcon(response.body().getWeather().get(0).getIcon());
                            applicationGlobalVariables.setTemp(response.body().getMain().getTemp().toString());
                            applicationGlobalVariables.setHumidity(response.body().getMain().getHumidity().toString());
                            applicationGlobalVariables.setPressure(response.body().getMain().getPressure().toString());
                            applicationGlobalVariables.setTempMax(response.body().getMain().getTempMax().toString());
                            applicationGlobalVariables.setTempMin(response.body().getMain().getTempMin().toString());
                            if(openNew){
                                openActivity();
                            }
                            break;
                        default:
                            break;
                    }
                }
                else {
                    try {
                        errorModel = new ErrorModel();
                        gson = new Gson();
                        errorModel = gson.fromJson(response.errorBody().string(), ErrorModel.class);
                    } catch (Exception e) {
                    }
                }
            }

            @Override
            public void onFailure(Call<RequestWeatherResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Failed",
                        Toast.LENGTH_LONG).show();
            }
        });

    }

}
