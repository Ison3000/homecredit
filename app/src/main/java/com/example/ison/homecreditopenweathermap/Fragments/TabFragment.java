package com.example.ison.homecreditopenweathermap.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ison.homecreditopenweathermap.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment extends Fragment implements ViewPager.OnPageChangeListener {

    private TabLayout mTabLayout;

    @SuppressWarnings("deprecation")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View tabView = inflater.inflate(R.layout.tab_layout, container, false);

        mTabLayout = (TabLayout) tabView.findViewById(R.id.tabs);
        ViewPager mViewPager = (ViewPager) tabView.findViewById(R.id.viewpager);
        mTabLayout.addTab(mTabLayout.newTab().setText("Buttons"));
        mTabLayout.addTab(mTabLayout.newTab().setText("List"));

        PagerAdapter mPagerAdapter = new PagerAdapter(getFragmentManager());
        mPagerAdapter.addFragment(new Button_Fragment());
        mPagerAdapter.addFragment(new List_Fragment());

        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mViewPager.addOnPageChangeListener(this);
        mTabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        return tabView;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        TabLayout.Tab tab0 = mTabLayout.getTabAt(0);
        TabLayout.Tab tab1 = mTabLayout.getTabAt(1);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    class PagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragments = new ArrayList<>();

        private PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        private void addFragment(Fragment fragment) {
            mFragments.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }
}
