package com.example.ison.homecreditopenweathermap;

import com.example.ison.homecreditopenweathermap.Constants.CommonConstants;
import com.example.ison.homecreditopenweathermap.POJO.RequestCredentials;
import com.example.ison.homecreditopenweathermap.POJO.RequestWeatherResponse;
import com.example.ison.homecreditopenweathermap.Utils.ApiUtils;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Interface {
    @GET("/data/2.5/weather")
//    @GET("/data/2.5/weather?q="+ CommonConstants.londonID + "&appid="+CommonConstants.APP_ID)
    Call<RequestWeatherResponse> requestWeather(@Query("q") String id, @Query("appid") String appid);
}


//    @GET("/maps/api/geocode/json?sensor=false")
//    void getPositionByZip(@Query("address") String address, Callback<String> cb);
//}