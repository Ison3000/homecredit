package com.example.ison.homecreditopenweathermap.Utils;

public class ApiUtils {
    public static String DOMAIN = "http://api.openweathermap.org";

    public static final String URL_REQUEST_WEATHER = "/data/2.5/weather";
    public static final String URL_ICON = "/img/w/";
}
